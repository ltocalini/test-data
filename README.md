#Introduction
I wanted to show you an interesting stack of technologies. I have been working with this stack in several projects.
As a full stack engineer I can use several others technologies but I think this a really trendy stack.


##Technologies
- ElasticSearch
- Flask
- Python 3
- React.js
- Chartjs
- docker-compose
- Rabbitmq
- Redis

#Using the solution
- Please install docker and docker-compose: https://docs.docker.com/compose/install/#master-builds
- run:
  ```
  docker-compose up
  ```
- go to http://127.0.0.1:8888/
- run:
  ```
  python3 process.py transactions.py
  ```
- You will see changes in realtime. After you can try to execute the same command but with big.json


#Interesting Urls

- ElasticSearch raw data: http://127.0.0.1:9200/transactions/_search?pretty
- ElasticSearch parsing errors: http://127.0.0.1:9200/errors/_search?pretty
- WS with the aggregation: http://127.0.0.1:8080/api/aggregate


