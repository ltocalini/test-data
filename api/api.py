import os
import json
from elasticsearch import Elasticsearch
import uuid
import datetime as dt
from netaddr import IPAddress
from flask import Flask, jsonify, request
from broker import send_payload

ES = Elasticsearch(host='elasticsearch')


class UserStatusSearch:

    RECORDS = [
        {'user_id': 1, 'created_at': '2017-01-01T10:00:00', 'status': 'paying'},
        {'user_id': 1, 'created_at': '2017-03-01T19:00:00', 'status': 'paying'},
        {'user_id': 1, 'created_at': '2017-02-01T12:00:00', 'status': 'cancelled'},
        {'user_id': 3, 'created_at': '2017-10-01T10:00:00', 'status': 'paying'},
        {'user_id': 3, 'created_at': '2016-02-01T05:00:00', 'status': 'cancelled'},
    ]

    def __init__(self):
        """
        Making a data_map structure for querying later in an easy way
        """
        self.data_map = {self.make_key(x): x['status'] for x in self.RECORDS}

    def make_key(self, record):
        return str(record['user_id']) + record['created_at']

    def get_status(self, user_id, date):
        key = '%s%s' % (user_id,  date)
        if key in self.data_map:
            return self.data_map[key]
        return 'non-paying'


class IpRangeSearch:

    RANGES = {
        'london': [
            {'start': '10.10.0.0', 'end': '10.10.255.255'},
            {'start': '192.168.1.0', 'end': '192.168.1.255'},
        ],
        'madrid': [
            {'start': '172.16.13.0', 'end': '172.16.13.255'},
        ],
        'munich': [
            {'start': '10.12.0.0', 'end': '10.12.255.255'},
            {'start': '172.16.10.0', 'end': '172.16.11.255'},
            {'start': '192.168.2.0', 'end': '192.168.2.255'},
        ]
    }

    def __init__(self):
        """
        Making a data_map structure for querying later in an easy way
        """
        self.data_map = []
        for city in self.RANGES:
            for record in self.RANGES[city]:
                record['start'] = IPAddress(record['start'])
                record['end'] = IPAddress(record['end'])
                record['city'] = city
                self.data_map.append(record)

    def get_city(self, ip):
        ip = IPAddress(ip)
        for record in self.data_map:
            if record['start'] <= ip <= record['end']:
                return record['city']
        return '**unknown**'


app = Flask(__name__)

UPLOAD_FOLDER = os.getenv('UPLOAD_PATH', '/uploads/')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

user_status_search = UserStatusSearch()
ip_range_search = IpRangeSearch()


@app.route('/user_status/<user_id>')
def user_status(user_id):
    """
    Return user status for a give date

    /user_status/1?date=2017-10-10T10:00:00
    """
    date = dt.datetime.strptime(
        str(request.args.get('date')), '%Y-%m-%dT%H:%M:%S').isoformat()

    return jsonify({'user_status': user_status_search.get_status(int(user_id), date)})


@app.route('/ip_city/<ip>')
def ip_city(ip):
    """
    Return city for a given ip

    /ip_city/10.0.0.0
    """
    return jsonify({'city': ip_range_search.get_city(ip)})


def get_aggregation():
    """
    query to Elasticsearch
    """
    query = {
        "aggs": {
            "city": {
                "terms": {
                    "field": "city.keyword"
                },
                "aggs": {
                    "user_status": {
                        "terms": {
                            "field": "user_status.keyword"
                        },
                        "aggs": {
                            "product_price":  {
                                "sum": {
                                    "field": "product_price"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    res = ES.search(index='transactions', body=query)
    output = []
    for agg in res['aggregations']['city']['buckets']:
        city = agg['key']
        for bucket in agg['user_status']['buckets']:
            user_status = bucket['key']
            product_price = bucket['product_price']['value']
            output.append(
                {'city': city, 'user_status': user_status, 'sum': product_price})
    return output


def get_aggregation_by_time():
    """
    query to Elasticsearch
    """
    query = {
        "size": 1200,
        "aggs": {
            "by_time": {
                "date_histogram": {
                    "field": "created_at",
                    "interval": "hour"
                },
                "aggs": {
                    "user_status": {
                        "terms": {
                            "field": "user_status.keyword"
                        },
                        "aggs": {
                            "product_price":  {
                                "sum": {
                                    "field": "product_price"
                                }
                            },
                        }
                    }
                }
            }
        }
    }

    res = ES.search(index='transactions', body=query)
    output = []
    for agg in res['aggregations']['by_time']['buckets']:
        key = agg['key_as_string']
        for bucket in agg['user_status']['buckets']:
            user_status = bucket['key']
            product_price = bucket['product_price']['value']
            output.append(
                {'time': key, 'user_status': user_status, 'sum': product_price})
    return output


@app.route('/aggregate/time')
@app.route('/api/aggregate/time')
def aggregate_by_time():
    return jsonify(get_aggregation_by_time())


@app.route('/aggregate')
@app.route('/api/aggregate')
def aggregate():
    return jsonify(get_aggregation())


@app.route('/api/charts')
def charts():
    aggs = get_aggregation()
    data = {'charts': {}}
    pies = {}
    colors = ['#FF6384', '#36A2EB', '#FFCE56', ]

    for row in aggs:
        if row['city'] not in pies:
            pies[row['city']] = {
                'title': row['city'],
                'chart': {
                    'labels': [],
                    'datasets': [
                        {
                            'data': [],
                            'backgroundColor':colors
                        }
                    ]
                }
            }
        pies[row['city']]['chart']['labels'].append(row['user_status'])
        pies[row['city']]['chart']['datasets'][0]['data'].append(row['sum'])
    data['charts']['pies'] = list(pies.values())
    by_time = {}
    status = []
    for record in get_aggregation_by_time():
        if record['time'] not in by_time:
            by_time[record['time']] = {}
        by_time[record['time']][record['user_status']] = record['sum']
        status.append(record['user_status'])

    labels = list(by_time.keys())
    status = list(set(status))
    datasets = []
    for idx, user_status in enumerate(status):
        tmp = {
            'label': user_status,
            'data': [],
            'fill': False,
            'lineTension': 0.1,
            'backgroundColor': colors[idx],
            'borderColor':  colors[idx],
            'borderCapStyle': 'butt',
            'borderDash': [],
            'borderDashOffset': 0.0,
            'borderJoinStyle': 'miter',
            'pointBorderColor':  colors[idx],
            'pointBackgroundColor': '#fff',
            'pointBorderWidth': 1,
            'pointHoverRadius': 5,
            'pointHoverBackgroundColor':  colors[idx],
            'pointHoverBorderColor':  colors[idx],
            'pointHoverBorderWidth': 2,
            'pointRadius': 1,
            'pointHitRadius': 10,
        }
        for label in labels:
            val = 0
            if label in by_time and user_status in by_time[label]:
                val = by_time[label][user_status]
            tmp['data'].append(val)
        datasets.append(tmp)
    data['charts']['time'] = {'labels': labels,
                              'datasets': datasets}
    return jsonify(data)


@app.route('/ingest/', methods=['POST'])
@app.route('/api/ingest/', methods=['POST'])
def ingest():
    """
    Ingest new data to elasticsearch
    """
    if 'file' in request.files:
        tmp_file = request.files['file']
        filename = str(uuid.uuid4()).replace('-', '') + '.json'
        file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        tmp_file.save(file_path)
        with open(file_path) as infile:
            for line in infile:
                send_payload('process', {'data': line})
    else:
        doc = request.json
        send_payload('process', doc)
    return jsonify({})


if __name__ == '__main__':
    app.run(host='0.0.0.0')
