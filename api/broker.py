import uuid
from kombu import BrokerConnection, Connection, Exchange, Queue
from kombu.mixins import ConsumerMixin
from kombu.common import maybe_declare


# default rabbitmq uri pointing to default docker name
DEFAULT_URI = 'amqp://rabbitmq:5672/'


class Producer(object):
    def __init__(self, channel, uri=DEFAULT_URI, durable=False):
        self.uri = uri
        self.conn_type = 'fanout'
        self.exchange = channel
        self.routing = channel
        self.durable = durable
        self.queue = channel

    def publish(self, data):
        _exchange = Exchange(self.exchange, type=self.conn_type,
                             durable=self.durable)
        _queue = Queue(self.queue, _exchange)
        with BrokerConnection(self.uri, transport_options={
                'ttl': True}, timeout=0) as conn:
            with conn.SimpleQueue(_queue) as queue:
                queue.put(data)


class Consumer(ConsumerMixin):
    def __init__(self, callback, channels, conn_type='fanout',
                 uri=DEFAULT_URI, durable=False):

        self.connection = Connection(uri)
        self.callback = callback

        if type(channels) is not list:
            channels = [channels]

        self.queues = []

        for name in channels:
            exchange = Exchange(name, type=conn_type, durable=durable)
            queue = Queue(uuid.uuid4().hex,
                          exchange=exchange, routing_key=name)
            self.queues.append(queue)

    def get_consumers(self, Consumer, channel):
        return [Consumer(queues=self.queues,
                         callbacks=[self.callback])]


def send_payload(channel, payload, uri=DEFAULT_URI):
    producer = Producer(channel, uri)
    producer.publish(payload)
