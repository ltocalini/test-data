import datetime
import json
import random

IPS = ['10.10.10.10', '10.13.10.10', '192.168.2.1', '172.16.11.10', '10.12.15.15',
       '192.168.1.10', '10.13.10.10', '192.168.1.60', '10.10.30.30', '172.16.13.12']

base_json = {"click_id": None, "created_at": None, "product_id": None,
             "product_price": None, "user_id": None, "ip": None}

first_date = datetime.datetime.now() - datetime.timedelta(days=30)
for i in range(43200):
    date = first_date + datetime.timedelta(minutes=i)
    base_json['click_id'] = i
    base_json['created_at'] = date.strftime('%Y-%m-%dT%H:%M:00')
    base_json['product_id'] = random.randint(1000, 3000)
    base_json['user_id'] = random.randint(1, 3)
    base_json['product_price'] = random.randint(60, 460)
    base_json['ip'] = random.choice(IPS)
    print(json.dumps(base_json))
