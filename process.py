import json
import requests
import sys

if __name__ == '__main__':
    if len(sys.argv) > 1:
        file_path = sys.argv[-1]
        url = 'http://localhost:8080/ingest/'
        with open(file_path) as f:
            for line in f:
                r = requests.post(url, json={'data': line})
