import json
import hashlib
import requests
import traceback
from elasticsearch import Elasticsearch
from redis import Redis
from broker import Consumer, send_payload

BACKEND_URL = 'http://api:5000'
USER_URL = BACKEND_URL + '/user_status/%s?date=%s'
IP_URL = BACKEND_URL + '/ip_city/%s'

ES = Elasticsearch(host='elasticsearch')
REDIS = Redis(host='redis')


class cache(object):

    def __init__(self, f):
        self.f = f

    def __call__(self, *args, **kwargs):
        key = json.dumps(args) + json.dumps(kwargs) + self.f.__name__
        key = hashlib.md5(key.encode('utf-8')).hexdigest()
        val = REDIS.get(key)
        if val:
            return json.loads(val)
        print('cached')
        val = self.f(*args, **kwargs)
        REDIS.set(key, json.dumps(val))
        return val


@cache
def get_city(ip):
    ip_url = IP_URL % ip
    response = requests.get(ip_url).json()
    return response['city']


@cache
def get_user_status(user_id, date):
    user_url = USER_URL % (user_id, date)
    response = requests.get(user_url)
    return response.json()['user_status']


class Process(object):
    def on_message(self, payload, message):
        message.ack()
        try:
            record = json.loads(payload['data'])
            date = record['created_at']
            record['user_status'] = get_user_status(record['user_id'], date)
            record['city'] = get_city(record['ip'])
            res = ES.index(index='transactions',
                           doc_type='transactions', body=record, id=record['click_id'])

        except Exception as e:
            traceback.print_exc()
            data = {'data': payload['data'], 'error': str(e)}
            res = ES.index(index='errors',
                           doc_type='erros', body=data)


if __name__ == '__main__':  # pragma: no cover
    proc = Process()
    Consumer(proc.on_message, 'process').run()
