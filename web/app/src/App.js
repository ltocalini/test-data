import React, { Component } from 'react';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import TextField from 'material-ui/TextField';
import Reboot from 'material-ui/Reboot';
import BottomNavigation, { BottomNavigationAction } from 'material-ui/BottomNavigation';
import HelpIcon from 'material-ui-icons/Help';
import Modal from 'material-ui/Modal';
import Typography from 'material-ui/Typography';

import logo from './logo.svg';
import './App.css';
import  SimpleReactFileUpload from './Upload';
import Dashboard from './Dashboard';

import {grey, amber, red} from 'material-ui/colors'



const muiTheme = createMuiTheme({
    palette: {
      primary: grey,
      accent: amber,
      error: red,
      type: 'dark'
    }
})


class App extends Component {
  state = {
    open: false,
  };
  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  render() {
    const { classes } = this.props;

    return (
          <MuiThemeProvider theme={muiTheme}>
            <Reboot/>
            <BottomNavigation
              showLabels
              >

            </BottomNavigation>
            <div>

              <Dashboard />          
              <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.state.open}
                onClose={this.handleClose}
                >
                <div  className={this.props.paper}>
                  <Typography variant="title" id="modal-title">
                    Help
                  </Typography>
                  <Typography variant="body2" gutterBottom>
                    With this Dashboard you will be able to upload transaction files and render data.
                    
                  </Typography>
                </div>
              </Modal>
            </div>
            </MuiThemeProvider>


    );
  }
}

export default App;
