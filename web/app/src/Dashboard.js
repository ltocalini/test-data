import React from 'react';

import './App.css';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import { diff, addedDiff, deletedDiff, updatedDiff, detailedDiff } from 'deep-object-diff';
import {Pie, Line} from 'react-chartjs-2';






class Pies extends React.Component {
  render() {
      return (
          <Grid container spacing={40}>

            {this.props.data.map((item, idx) => (
                <Grid item xs={12} sm={6} >
                  <Paper className={this.props.paper}>
                    <Typography variant="headline" component="h3">
                      {item.title}
                    </Typography>
                    <Pie data={item.chart} />
                    
                  </Paper>
                </Grid>
            ))}
          </Grid>

              
      )
  }
    
}

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {'pies': [],
                  'time': {'labels': [], 'datasets': []}}
    this.updateData()  
  }
  updateData = () => {
    fetch('/api/charts')
          .then((response) => {
              if (!response.ok) {
                  setTimeout(this.updateData, 1000)
                  return false;
              }
              setTimeout(this.updateData, 1000)
              return response.json()

          })
          .then((data) => {
              if (data){
                  if (diff(this.state.pies, data['charts']['pies'])){
                      this.setState({pies: data['charts']['pies']})
                  }
                  if (diff(this.state.time, data['charts']['time'])){
                      this.setState({time: data['charts']['time']})
                  }
              }
              

          })

  }
  render() {
      return (
          <div className={this.props.root} style={{ padding: 20 }}>

            <Pies data={this.state.pies} />
            <Grid container spacing={40}  justify={'center'}>
              <Grid item xs={10} sm={10}>
                <Paper className={this.props.paper}>
                  <Typography variant="headline" component="h3">
                    Time Serie
                  </Typography>

                  <Line data={this.state.time}/>
                </Paper>
              </Grid>
            </Grid>
          </div>
              
      )
  }
}

export default Dashboard;

