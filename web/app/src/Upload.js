import React from 'react';
import './App.css';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';

import { post } from 'axios';


const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  exampleImageInput: {
    cursor: 'pointer',
    position: 'absolute',
    top: '0',
    bottom: '0',
    right: '0',
    left: '0',
    width: '100%',
    opacity: '0'
  }
});

class SimpleReactFileUpload extends React.Component {

  constructor(props) {
    const { classes } = props;
    super(props);


    this.state ={
      file:null
    }
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
    this.fileUpload = this.fileUpload.bind(this)
  }
  onFormSubmit(e){
    e.preventDefault() // Stop form submit
  }
  _openFileDialog(){
      var fileUploadDom = React.findDOMNode(this.refs.uploadFile);
      fileUploadDom.click();
  }
  onChange(e) {
      this.setState({file:e.target.files[0]})
      this.fileUpload(e.target.files[0])
  }
  fileUpload(file){
    const url = '/api/ingest/';
    const formData = new FormData();
    formData.append('file',file)
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    this.setState({file:null})
    return  post(url, formData,config)
  }

  render() {
      return (
          <div>
            <Button  color="primary" className={this.props.button} primary={true} onClick={this._openFileDialog}>
              <input class='input-file' id="uploadFile" style={styles.exampleImageInput} type="file" onChange={this.onChange} />                  
            </Button>
          </div>
      )
  }
}

export default withStyles(styles)(SimpleReactFileUpload);

